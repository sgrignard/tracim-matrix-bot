import argparse
import asyncio
import json
import typing

import aiohttp
from aiohttp_sse_client import client as sse_client
import bs4
import nio


async def get_tracim_ids(
    args: argparse.Namespace, session: aiohttp.ClientSession
) -> typing.Tuple[int, int]:
    async with session.get(f"{args.tracim_url}/auth/whoami") as request:
        # Get Tracim's user's id
        user = await request.json()
        tracim_user_id = user.get("user_id")
    async with session.get(
        f"{args.tracim_url}/contents/{args.tracim_thread_id}"
    ) as request:
        # Get Tracim's workspace id containing the thread
        thread = await request.json()
        tracim_workspace_id = thread.get("workspace_id")
    return (tracim_user_id, tracim_workspace_id)


async def forward_tracim_messages(
    args: argparse.Namespace,
    tracim_user_id: int,
    tracim_session: aiohttp.ClientSession,
    matrix_client: nio.AsyncClient,
    lock: asyncio.Lock,
    forwarded_comment_ids: typing.Set[int],
    forwarded_event_ids: typing.Set[str],
) -> None:
    messages_url = f"{args.tracim_url}/users/{tracim_user_id}/live_messages"
    async with sse_client.EventSource(
        messages_url, session=tracim_session
    ) as events:
        # Wait for TLM (Tracim Live Message)
        async for event in events:
            if event.type != "message":
                continue
            message = json.loads(event.data)
            fields = message["fields"]
            content = fields.get("content", {})
            async with lock:
                # Filter out unrelated messages
                if (
                    message.get("event_type") != "content.created.comment"
                    or content.get("parent_id") != args.tracim_thread_id
                    or content.get("content_id") in forwarded_comment_ids
                ):
                    continue
                comment = bs4.BeautifulSoup(
                    content.get("raw_content"),
                    "html.parser"
                ).get_text()
                forwarded_comment_ids.add(content.get("content_id"))
                print(f"Got comment from tracim: {comment}")
                await matrix_client.room_send(
                    room_id=args.matrix_room_id,
                    message_type="m.room.message",
                    content={
                        "msgtype": "m.text",
                        "body": comment
                    }
                )


async def forward_matrix_message(
    args: argparse.Namespace,
    tracim_session: aiohttp.ClientSession,
    tracim_workspace_id: int,
    event: nio.RoomMessageText,
    lock: asyncio.Lock,
    forwarded_comment_ids: typing.Set[int],
    forwarded_event_ids: typing.Set[str],
) -> None:
    comment = {"raw_content": event.body}
    post_message_url = f"{args.tracim_url}/workspaces/{tracim_workspace_id}/contents/{args.tracim_thread_id}/comments"
    async with lock:
        if event.event_id not in forwarded_event_ids:
            async with tracim_session.post(
                    post_message_url, json=comment
            ) as send_message:
                print(f"Got message from matrix: {event.body}")
                forwarded_event_ids.add(event.event_id)
                message = await send_message.json()
                forwarded_comment_ids.add(message.get("content_id"))


async def run_bot(args: argparse.Namespace, forwarded_comment_ids: typing.Set[int], forwarded_event_ids: typing.Set[str]) -> None:
    # lock and set of comment ids forwarded from matrix
    # to avoid a cycle in the message forwarding
    lock = asyncio.Lock()
    timeout = aiohttp.ClientTimeout(total=0)
    async with aiohttp.ClientSession(
        auth=aiohttp.BasicAuth(args.tracim_user, args.tracim_password),
        timeout=timeout,
    ) as session:
        (tracim_user_id, tracim_workspace_id) = await get_tracim_ids(
            args, session
        )
        print(
            f"Tracim user id={tracim_user_id}, workspace id={tracim_workspace_id}"
        )

        async def forward_matrix(
            room: nio.MatrixRoom, event: nio.RoomMessageText
        ) -> None:
            if room.room_id != args.matrix_room_id:
                return
            await forward_matrix_message(
                args,
                session,
                tracim_workspace_id,
                event,
                lock,
                forwarded_comment_ids,
                forwarded_event_ids,
            )

        matrix_client = nio.AsyncClient(args.matrix_url, args.matrix_user)
        matrix_client.add_event_callback(forward_matrix, nio.RoomMessageText)
        await matrix_client.login(args.matrix_password)
        await asyncio.gather(
            matrix_client.sync_forever(timeout=30000),
            forward_tracim_messages(
                args,
                tracim_user_id,
                session,
                matrix_client,
                lock,
                forwarded_comment_ids,
                forwarded_event_ids,
            ),
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("matrix_user")
    parser.add_argument("matrix_password")
    parser.add_argument("matrix_url")
    parser.add_argument("matrix_room_id")
    parser.add_argument("tracim_user")
    parser.add_argument("tracim_password")
    parser.add_argument("tracim_url")
    parser.add_argument("tracim_thread_id", type=int)
    args = parser.parse_args()
    forwarded_comment_ids: typing.Set[int]
    forwarded_event_ids: typing.Set[str]
    try:
        with open(".bot-forwarded.json", "r") as forwarded_file:
            forwarded = json.load(forwarded_file)
            forwarded_comment_ids = set(forwarded["comment_ids"])
            forwarded_event_ids = set(forwarded["event_ids"])
    except FileNotFoundError:
        forwarded_comment_ids = set()
        forwarded_event_ids = set()
    try:
        asyncio.run(run_bot(args, forwarded_comment_ids, forwarded_event_ids))
    finally:
        with open(".bot-forwarded.json", "w") as forwarded_file:
            json.dump(
                {
                    "comment_ids": list(forwarded_comment_ids),
                    "event_ids": list(forwarded_event_ids),
                },
                forwarded_file
            )
